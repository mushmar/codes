import { api } from '@briskly/shared';

// получить торговые точки по бренду
// обновить компани бренд
// создать бренд
// удалить бренд
//

class LogoAPI {
  getList(opts: Record<string, any>) {
    return api.endpoints.current.get({ path: 'dashboard/company-brand/get-list', params: opts });
  }

  getById(opts: Record<string, any>) {
    return api.endpoints.current.get({ path: 'dashboard/company-brand/get-by-id', params: opts });
  }

  update(opts: Record<string, any>) {
    return api.endpoints.current.post({ path: 'dashboard/company-brand/update', params: opts });
  }

  create(opts: Record<string, any>) {
    return api.endpoints.current.post({ path: 'dashboard/company-brand/create', params: opts });
  }

  uploadLogo(opts: Record<string, any>) {
    return api.endpoints.current.post({ path: 'dashboard/company-brand/logo/upload', params: opts });
  }

  deleteLogo(opts: Record<string, any>) {
    return api.endpoints.current.post({ path: 'dashboard/company-brand/logo/delete', params: opts });
  }

  remove(opts: Record<string, any>) {
    return api.endpoints.current.post({ path: 'dashboard/company-brand/delete', params: opts });
  }
}

export const logoAPI = new LogoAPI();
