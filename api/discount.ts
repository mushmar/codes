import { api } from '@briskly/shared';

class DiscountAPI {
  getList(opts) {
    const { limit = 20, page = 1, order, q = '' } = opts;

    return api.endpoints.current.get({ path: 'dashboard/discount-set/get-list', params: { limit, page, order, q } });
  }

  update(opts) {
    return api.endpoints.current.post({ path: 'dashboard/discount-set/update', params: opts });
  }
  updateBrand(opts) {
    return api.endpoints.current.post({ path: 'dashboard/company/upgrade', params: opts });
  }
  delete(opts) {
    const { id } = opts;
    return api.endpoints.current.post({ path: 'dashboard/discount-set/delete', params: { id } });
  }

  uploadImage(opts) {
    const { file, id } = opts;
    return api.endpoints.current.post({ path: 'dashboard/discount-set/image/upload', params: { id, file } });
  }
}

export const discountAPI = new DiscountAPI();
